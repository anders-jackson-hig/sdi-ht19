package se.hig.jackson.cxf.service;

//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;

import javax.jws.WebService;

import se.hig.jackson.cxf.data.Person;

@WebService(name="TestBean", endpointInterface = "se.hig.jackson.cxf.service.TestWS")
public class TestWSImpl implements TestWS {
	
	private Person data[] = { new Person("A", "J", 42), new Person("O", "J", 46) };
	
	@Override
	public int add(int a, int b) {
		return a + b;
	}

	@Override
	public String hello(String name) {
		return "Hello " + name;
	}

	@Override
	public String helloToYou() {
		return "Hello world";
	}

	@Override
	public Person getPerson(int index) {
		Person returnValue = null;
		switch (index) {
		case 1:
			returnValue = data[0];
			break;
		case 2:
			returnValue = data[1];
			break;
		default:
			returnValue = null;
			break;
		}
		return returnValue;
	}

	@Override
	public String getLicenseId(Person p) {
		return p.getFirstName()+" "+p.getLastName();
	}

	@Override
	public boolean setAllPersons(Person [] personer) {
		// Exempel på hur man enkelt kan konvertera mellan array och List
		// Läs själv om tillkortakommanden med respektivt steg.
		//
		//List<Person> personLista = Arrays.asList(personer);
		//List<Person> person2Lista = new ArrayList<>(personLista);
		//Person [] pers = personLista.toArray(new Person[personLista.size()]);
		//result = någonMetod(personLista);
		
		boolean result = false; 		
		return result;
	}

	@Override
	public Person[] getAllPersons() {
		return data;
	}
	@Override
	public Person[] getAllPersonsMax(int max) {
		return data;
	}

}
