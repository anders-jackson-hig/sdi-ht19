package se.hig.jackson.cxf.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import se.hig.jackson.cxf.data.Person;

/**
 * En SEI för en Web Service som är ett demo.
 * 
 * Dessa rutiner skall ge några välkomstfraser, addera två tal samt ge olika Java Bean beroende på index.
 * 
 * @author <a href="mailto:anders.jackson@hig.se">Anders Jackson, anders.jackson@hig.se</a>
 * @version 2018-09-20
 * @version 2019-10-01
 * 
 */
@WebService(name="TestBean", targetNamespace="http://service.cxf.jackson.hig.se/")
public interface TestWS {
	/**
	 * Hämtar en Java Bean beroende på index.
	 * @param index index till Java Bean
	 * @return data
	 */
	@WebMethod(operationName = "getPerson", action = "urn:GetPerson")
	public Person getPerson(@WebParam(name = "index") int index);
	/**
	 * Adderar två heltal och returnerar summan.
	 * @param a tal ett
	 * @param b tal två
	 * @return summan
	 */
	@WebMethod(operationName = "add", action = "urn:Add")
	public int add(@WebParam(name = "a") int a, @WebParam(name = "b") int b);
	/**
	 * Skapar ett välkomnstmeddelande till namnet som anges.
	 * @param name namnet på den som skall hälsas välkomnen
	 * @return hälsningsfras
	 */
	@WebMethod(operationName = "hello", action = "urn:Hello")
	public String hello(@WebParam(name = "name") String name);
	/**
	 * Returnerar en generell hälsningsfras.
	 * @return en hälsningsfras
	 */
	@WebMethod(operationName = "helloToYou", action = "urn:HelloToYou")
	public String helloToYou();
	
	@WebMethod
	public String getLicenseId(@WebParam(name="person") Person p);
	@WebMethod
	public boolean setAllPersons(@WebParam(name = "persons") Person [] p);
	@WebMethod
	public Person [] getAllPersons();
	@WebMethod
	public Person [] getAllPersonsMax(@WebParam(name = "maxPersons") int maxPersons);
}
