/**
 * 
 */
package se.hig.jackson.cxf.data;

import java.io.Serializable;

/**
 * Detta är ett exempel på en klass för att överföra data, dvs sk Java Bean.
 * 
 * En Java Bean skall ha följande egenskaper.
 * <ul>
 *  <li> Det skall ha en default constructor som inte gör något.
 *  <li> Den skall implementera Serializable.
 *  <li> Alla attributen skall vara tillgängliga med geters/seters ELLER vara public.
 *  <li> Accessmetoderna får inte göra några beräkningar.
 * </ul>
 * 
 * @author <a href="">Anders Jackson, anders.jackson@hig.se</a>
 * @version 2018-09-20
 * @version 2019-10-01
 *
 */
public class Person implements Serializable {
	/**
	 * Serienummer som genereras från hur DataBean ser ut.
	 * Förändras innehållet, så måste denna ändras.
	 */
	private static final long serialVersionUID = -5460110133510609467L;
	
	/*
	 * Förnamn
	 */
	private String firstName;
	/*
	 * Efternamn
	 */
	private String lastName;
	/*
	 * Skonummer
	 */
	private int shoeSize;
	
	/**
	 * Default konstruktor som är tom.
	 */
	public Person() {
		
	}
	/**
	 * Konstruktor som bara finns här i servern.
	 * 
	 */
	public Person(String firstName, String lastName,int shoeSize) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.shoeSize = shoeSize;
	}
	/**
	 * Hämta förnamn
	 * @return förnamn
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * Sätt förnamn
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * Hämta efternamn
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * Sätt efternamn
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * Hämta skonummer
	 * @return shoeSize
	 */
	public int getShoeSize() {
		return shoeSize;
	}
	/**
	 * Sätter skonummer
	 * @param shoeSize
	 */
	public void setShoeSize(int shoeSize) {
		this.shoeSize = shoeSize;
	}
}
