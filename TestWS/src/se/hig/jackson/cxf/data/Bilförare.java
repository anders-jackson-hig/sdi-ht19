package se.hig.jackson.cxf.data;

import java.io.Serializable;

public class Bilförare implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String körkortsID;
	
	private Person bilförare;

	public Bilförare() {}
	
	public String getKörkortsID() {
		return körkortsID;
	}

	public void setKörkortsID(String körkortsID) {
		this.körkortsID = körkortsID;
	}

	public Person getBilförare() {
		return bilförare;
	}

	public void setBilförare(Person bilförare) {
		this.bilförare = bilförare;
	}

}
