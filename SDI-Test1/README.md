#Eclipse och CFX.

- Installera en Java Servlet Container (exempelvis Tomcat).
- Ladda ned CXF 2.7.18 / senaste versionen i 2.x, kopiera till $HOME/Java/ och packa 
   upp den där.
- Lägg till i "Window > Preferences > Web Services > CXF 2.x preferences > CXF Runtime" 
   den katalog som CXF packats upp i ($HOME/Java/) samt bocka för den.
- 1) I "Web Services > CXF 2.x preferences > Endpoint Config" välj "Use Spring 
     application context".
  2) I "Web Services > Server and Runtime" välj lämplig Java Servlet Container (exempelvis 
     Tomcat) och CXF.
- Skapa ett webprojekt med "File > New > Dynamic Web Project" med namnet 
   TestWS (eller liknande)
- Högerklicka på projektet och välj "Properties"
- 1) Välj "Project Facets"
  2) Bocka för "CXF 2.x Web Services", och klicka "Apply and Close"
- Skapa ett SEI, dvs ett Java Interface med de metoder som man vill ha som Java Services. 
   Exempelvis "se.hig.service.TestWS"
- Markera interfacet med @WebService(targetNamespace="http://service.hig.se/")
- Skapa en implementation av WS, "se.hig.service.TestWSImpl" från interfacet.  
   Markera klassen med @WebService(endpointInterface = "se.hig.service.TestWS")
- Högerklicka TestWS-interfacet och välj "Web Service > Create Web Service".
- Välj "Bottom up" och se till att rätt "Java Servlet Container" och "Web Service Runtime" 
   är valda. Klicka på Next.
- Välj implementationen, sedan Next eller Finish.

Tomcaten behöver kanske startas om.
Öppna webklient och öppna "http://localhost:8080/TestWS/services/" så hittar man WSDL-filen.

Använd WSDL-URL:en till "Web Services Explorer" och prova Web Service:en som skrivits.

Om man bygger om SEI, så måste man generera ny kod med "Web Service > Create Web Service".  
Samt att man kan behöve manuellt starta om Java Servlet Container:en

(Det skall stämma på ett ungefär).

När CXF genererar WS, så kan man även klicka på att en exempelclient skall skapas.
Man behöver justera följande rad så att URL:en stämmer (skall gå till service och Porten)
Exempelvis från
  service.addPort(portName, SOAPBinding.SOAP11HTTP_BINDING,
                        "http://localhost:9090/TestBeanImplPort");
till
  service.addPort(portName, SOAPBinding.SOAP11HTTP_BINDING,
                        "http://localhost:8080/TestWS/services/TestBeanImplPort");

Sedan är det bara att lägga till anrop via "client"-proxy-objektet samt exekvera som om det 
vore en vanlig Java application.

/Jackson
